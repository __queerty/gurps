using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Gurps
{
	class CharacterCreateDialog : Form
	{
		private Database Database;
		private TableLayoutPanel TableLayoutPanel;
		private ComboBox CharacterList;
		private Dictionary<string, NpgsqlDbType> ColumnNameToType;
		private string characterName;
		
		public string CharacterName
		{
			get
			{
				return characterName.Clone() as string;
			}
		}
		
		public CharacterCreateDialog(Database Database)
		{
			this.Database = Database;
			
			this.Text = "Create Character";
			
			this.TableLayoutPanel = new TableLayoutPanel();
			this.TableLayoutPanel.GrowStyle = TableLayoutPanelGrowStyle.AddRows;
			this.TableLayoutPanel.Size = this.ClientSize;
			
			this.Controls.Add(TableLayoutPanel);
			List<string> CharacterFields = this.Database.GetColumns("Characters","Characters");
			foreach(string Field in CharacterFields)
			{
				TableLayoutPanel tlp = new TableLayoutPanel();
				tlp.ColumnCount = 2;
				tlp.RowCount = 1;
				Label l = new Label();
				l.Name = "lbl" + Field;
				l.Text = Field;
				TextBox t = new TextBox();
				t.Name = "txt" + Field;
				tlp.Controls.Add(l);
				tlp.Controls.Add(t);
				tlp.Height = Math.Max(l.Height, t.Height);
				Console.WriteLine(tlp.Size);
				this.TableLayoutPanel.Controls.Add(tlp);
			}
			Button b = new Button();
			b.Text = "Save";
			b.Name = "btnSave";
			b.Click += new EventHandler(this.SaveButton_Click);
			this.TableLayoutPanel.Controls.Add(b);
			
			this.StartPosition = FormStartPosition.CenterParent;
		}
		
		private void SaveButton_Click(object sender, EventArgs e)
		{
			string Name = (this.Controls.Find("txtName",true)[0] as TextBox).Text;
			int PointsUsed = int.Parse((this.Controls.Find("txtPoints Used",true)[0] as TextBox).Text);
			int PointsFree = int.Parse((this.Controls.Find("txtPoints Free",true)[0] as TextBox).Text);
			int WeightPounds = int.Parse((this.Controls.Find("txtWeight Pounds",true)[0] as TextBox).Text);
			int HeightInches = int.Parse((this.Controls.Find("txtHeight Inches",true)[0] as TextBox).Text);
			decimal Wealth = decimal.Parse((this.Controls.Find("txtWealth",true)[0] as TextBox).Text);
			if(this.Database.InsertCharacter(Name, PointsUsed, PointsFree, WeightPounds, HeightInches, Wealth) == 0)
				MessageBox.Show("Could not create character", "Error");
			else
			{
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}
	}
}