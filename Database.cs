using System;
using System.Data;
using System.Collections.Generic;
using Npgsql;
using NpgsqlTypes;

namespace Gurps
{
	class Database
	{
		private NpgsqlConnectionStringBuilder ConnectionString;
		private NpgsqlConnection Connection;
		public Database()
		{
			this.ConnectionString = new NpgsqlConnectionStringBuilder();
			this.ConnectionString.Host = "localhost";
			this.ConnectionString.Database = "gurps";
			this.ConnectionString.UserName = "roleplay";
			this.ConnectionString.Password = "AA5vH1#deF8&392";
			this.Connection = new NpgsqlConnection(this.ConnectionString.ToString());
		}
		
		#region "Initialization and database schema queries"
		public void Initialize()
		{
			this.Connection.Open();
		}
		#endregion
		
		#region "Database Calls"
		public int InsertImage(string Name, byte[] Image)
		{
			string sql = "INSERT INTO \"Characters\".\"Images\" VALUES(:Name, :Image)";
			int RowsAffected = 0;
			using(NpgsqlCommand Command = new NpgsqlCommand(sql, this.Connection))
			{
				Command.Parameters.Add(new NpgsqlParameter("Name", Name));
				NpgsqlParameter ImageParameter = new NpgsqlParameter("Image", NpgsqlDbType.Bytea);
				ImageParameter.Value = Image;
				Command.Parameters.Add(ImageParameter);
				RowsAffected = Command.ExecuteNonQuery();
			}
			return RowsAffected;
		}
		
		public byte[] SelectImage(string Name)
		{
			string sql = "SELECT \"Image\"::bytea FROM \"Characters\".\"Images\" WHERE \"Character\" = :Name";
			byte[] Data = null;
			using(NpgsqlCommand Command = new NpgsqlCommand(sql, this.Connection))
			{
				Command.Parameters.Add(new NpgsqlParameter("Name", Name));
				Data = Command.ExecuteScalar() as byte[];
			}
			return Data;
		}
		public int InsertCharacter(string Name, int PointsUsed, int PointsFree, int WeightPounds, int HeightInches, decimal Wealth)
		{
			int RowsAffected = 0;
			string sql = "INSERT INTO \"Characters\".\"Characters\" SELECT :Name, :PointsFree, :PointsUsed, :WeightPounds, :HeightInches, :Wealth";
			using(NpgsqlCommand Command = new NpgsqlCommand(sql, this.Connection))
			{
				Command.Parameters.Add(new NpgsqlParameter("Name", Name));
				Command.Parameters.Add(new NpgsqlParameter("PointsUsed", PointsUsed));
				Command.Parameters.Add(new NpgsqlParameter("PointsFree", PointsFree));
				Command.Parameters.Add(new NpgsqlParameter("WeightPounds", WeightPounds));
				Command.Parameters.Add(new NpgsqlParameter("HeightInches", HeightInches));
				NpgsqlParameter pWealth = new NpgsqlParameter("Wealth", NpgsqlDbType.Money);
				pWealth.Value = Wealth;
				Command.Parameters.Add(pWealth);
				
				RowsAffected = Command.ExecuteNonQuery();
			}
			return RowsAffected;
		}
		public int UpdateCharacter(string Name, int PointsUsed, int PointsFree, int WeightPounds, int HeightInches, decimal Wealth)
		{
			throw new NotImplementedException();
		}
		
		public List<string> SelectAllCharacters()
		{
			List<string> Characters = new List<string>();
			string sql = "SELECT \"Name\" FROM \"Characters\".\"Characters\"";
			using(NpgsqlCommand Command = new NpgsqlCommand(sql, this.Connection))
			{
				using(NpgsqlDataReader Reader = Command.ExecuteReader())
				{
					while(Reader.Read())
					{
						Characters.Add(Reader["Name"] as string);
					}
				}
			}
			return Characters;
		}
		#endregion
		public List<string> GetColumns(string Schema, string Table)
		{
			if(Schema == null || Schema.Equals("")) 
				throw new Exception("No schema selected");
			if(Table == null || Table.Equals(""))
				throw new Exception("No table selected");
			string sql = "SELECT column_name FROM information_schema.columns where table_schema = :Schema and table_name = :Table";
			List<string> s = new List<string>();
			using(NpgsqlCommand Command = new NpgsqlCommand(sql, this.Connection))
			{
				Command.Parameters.Add(new NpgsqlParameter("Schema", Schema));
				Command.Parameters.Add(new NpgsqlParameter("Table", Table));
				using(NpgsqlDataReader Reader = Command.ExecuteReader())
				{
					if(!Reader.HasRows)
						throw new Exception("No such schema or table: " + Schema + "." + Table);
					while(Reader.Read())
					{
						s.Add(Reader["column_name"] as string);
					}
				}
			}
			return s;
		}
	}
}