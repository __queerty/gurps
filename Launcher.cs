using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Gurps
{
	class Launcher : Form
	{
		private const string Version = "0.1.1";
		private Database Database;
		private CharacterData Character;
		private const double AspectRatio = 8.5/11.0;
		
		//Controls
		protected MainMenu MainMenu;
		
		public Launcher(Database Database)
		{
			this.Database = Database;
			this.Height = 1000;
			this.Width = (int)((double)this.Height*AspectRatio);
			this.FormBorderStyle = FormBorderStyle.FixedSingle;
			
			// Menus
			MainMenu = new MainMenu();
			this.Menu = MainMenu;
			// File Menu
			MenuItem FileMenu = new MenuItem();
			FileMenu.Text = "File";
			MainMenu.MenuItems.Add(FileMenu);
			// File Menu (Itme: New)
			MenuItem FileNew = new MenuItem();
			FileNew.Text = "New";
			FileNew.Click += new EventHandler(this.FileNew_Click);
			FileMenu.MenuItems.Add(FileNew);
			// File Menu (Item: Open)
			MenuItem FileOpen = new MenuItem();
			FileOpen.Text = "Open";
			FileOpen.Click += new EventHandler(this.FileOpen_Click);
			FileMenu.MenuItems.Add(FileOpen);
			// File Menu (Item: Exit)
			MenuItem FileExit = new MenuItem();
			FileExit.Text = "Exit";
			FileExit.Click += new EventHandler(this.FileExit_Click);
			FileMenu.MenuItems.Add(FileExit);
			
			// Image Region
			PictureBox pb = new PictureBox();
			pb.Name = "pbxImage";
			
			this.Controls.Add(pb);
			// Attributes Region
			TableLayoutPanel tlp1 = new TableLayoutPanel();
			tlp1.Name = "tlpAttributes";

			this.Controls.Add(tlp1);
			// Combat Region
			TableLayoutPanel tlp2 = new TableLayoutPanel();
			tlp2.Name = "tlpCombat";

			this.Controls.Add(tlp2);
			// Skills Region
			TableLayoutPanel tlp3 = new TableLayoutPanel();
			tlp3.Name = "tlpSkills";

			this.Controls.Add(tlp3);
			// Notes Region
			TableLayoutPanel tlp4 = new TableLayoutPanel();
			tlp4.Name = "tlpNotes";

			this.Controls.Add(tlp4);
			
			this.Layout += new LayoutEventHandler(this.DoLayout);
			
			
			this.Text = "Gurps Character Manager " + Version;
			this.Character = new CharacterData();
		}
		
		private void DoLayout(object sender, LayoutEventArgs e)
		{
			// Image Box
			PictureBox pbxImage = this.Controls.Find("pbxImage",true)[0] as PictureBox;
			pbxImage.Size = new Size((int)(this.Width * 0.40), (int)(this.Height * 0.40));
			pbxImage.DoubleClick += new EventHandler(this.pbxImage_DoubleClick);
			
			// Attributes Region
			TableLayoutPanel tlpAttributes = this.Controls.Find("tlpAttributes",true)[0] as TableLayoutPanel;
			tlpAttributes.Location = new Point(pbxImage.Right,pbxImage.Top);
			tlpAttributes.Size = new Size(this.Width - pbxImage.Width, pbxImage.Height);
			tlpAttributes.BackColor = Color.Blue;
			
			// Combat Region
			TableLayoutPanel tlpCombat = this.Controls.Find("tlpCombat",true)[0] as TableLayoutPanel;
			tlpCombat.Location = new Point(pbxImage.Left,tlpAttributes.Bottom);
			tlpCombat.Size = new Size(this.Width, (int)((this.Height - pbxImage.Height)/3.0));
			tlpCombat.BackColor = Color.Red;
			
			// Skills Region
			TableLayoutPanel tlpSkills = this.Controls.Find("tlpSkills",true)[0] as TableLayoutPanel;
			tlpSkills.Location = new Point(tlpCombat.Left,tlpCombat.Bottom);
			tlpSkills.Size = new Size(this.Width, (int)((this.Height - pbxImage.Height)/3.0));
			tlpSkills.BackColor = Color.Green;
			
			// Notes Region
			TableLayoutPanel tlpNotes = this.Controls.Find("tlpNotes",true)[0] as TableLayoutPanel;
			tlpNotes.Location = new Point(tlpSkills.Left,tlpSkills.Bottom);
			tlpNotes.Size = new Size(this.Width, (int)((this.Height - pbxImage.Height)/3.0));
			tlpNotes.BackColor = Color.Pink;
		}
		
		private void FileNew_Click(object sender, EventArgs e)
		{
			CharacterCreateDialog CreateCharacter = new CharacterCreateDialog(this.Database);
			DialogResult Result = CreateCharacter.ShowDialog();
			if(Result == DialogResult.OK)
			{
				this.Character.Name = (CreateCharacter.Controls.Find("Name",true)[0] as TextBox).Text;
				Console.WriteLine(this.Character.Name);
			}
			
		}
		
		private void FileOpen_Click(object sender, EventArgs e)
		{
			CharacterLoadDialog LoadCharacter = new CharacterLoadDialog(this.Database);
			DialogResult Result = LoadCharacter.ShowDialog();
			if(Result == DialogResult.OK)
			{
				Character.Name = LoadCharacter.CharacterSelected;
				this.SetImage(Character.Name);
			}
		}
		
		private void FileExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
		
		private void LoadImageButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog Load = new OpenFileDialog();
			DialogResult Result = Load.ShowDialog();
			if(Result == DialogResult.OK)
			{
				Console.WriteLine(Load.FileName);
			}
		}
		
		private void pbxImage_DoubleClick(object sender, EventArgs e)
		{
			if(Character.Name != null)
			{
				OpenFileDialog Open = new OpenFileDialog();
				DialogResult Result = Open.ShowDialog();
			}
		}
		
		private void TestImageLoad()
		{
			using(FileStream File = new FileStream("/home/queerty/Ubuntu One/Alicia Squires.jpg", FileMode.Open))
			{
				Console.WriteLine("Inserting image with size: " + File.Length);
				byte[] Data = new byte[File.Length];
				int Read = File.Read(Data, 0, (int)File.Length);
				if(Read != File.Length) throw new Exception("fuck");
				this.Database.InsertImage("Alice Squires", Data);
			}
		}
		
		private void SetImage(string Name)
		{
			Console.WriteLine(Name);
			byte[] Data = Database.SelectImage(Name);
			if(Data == null)
			{
				Console.WriteLine("Data == null");	
				return;
			}
			MemoryStream MemoryStream = new MemoryStream(Data);
			PictureBox pbxImage = this.Controls.Find("pbxImage",true)[0] as PictureBox;
			pbxImage.Image = Image.FromStream(MemoryStream);
		}
	}
}