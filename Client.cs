using System;
using System.Windows.Forms;

namespace Gurps
{
	class Gurps
	{
		public Database Database;
		
		public Gurps()
		{
			this.Database = new Database();
			this.Database.Initialize();
		}
		
		public static void Main(string[] args)
		{
			Console.WriteLine("Connecting to the database");
			Gurps Gurps = new Gurps();
			Application.Run(new Launcher(Gurps.Database));
		}
	}
}