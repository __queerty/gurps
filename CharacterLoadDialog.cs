using System;
using System.Data;
using System.Windows.Forms;

namespace Gurps
{
	class CharacterLoadDialog : Form
	{
		private Database Database;
		private TableLayoutPanel TableLayoutPanel;
		private ComboBox CharacterList;
		public string CharacterSelected;
		
		public CharacterLoadDialog(Database Database)
		{
			this.Database = Database;
			
			this.Text = "Load Character";
			
			this.TableLayoutPanel = new TableLayoutPanel();
			this.Controls.Add(TableLayoutPanel);
		
			ComboBox Characters = new ComboBox();
			Characters.Name = "Characters";
			foreach(string Name in this.Database.SelectAllCharacters())
			{
				Characters.Items.Add(Name);
			}
			this.TableLayoutPanel.Controls.Add(Characters);
			
			Button b = new Button();
			b.Name = "Open";
			b.Text = "Open";
			b.Click += new EventHandler(this.OpenButton_Click);
			this.TableLayoutPanel.Controls.Add(b);
			
			this.StartPosition = FormStartPosition.CenterParent;
		}
		
		private void OpenButton_Click(object sender, EventArgs e)
		{
			ComboBox Characters = this.Controls.Find("Characters", true)[0] as ComboBox;
			string Item = Characters.SelectedItem as string;
			if(Item != null && !Item.Equals(""))
			{
				CharacterSelected = Item;
				this.DialogResult = DialogResult.OK;
				this.Close();
			}
		}
	}
}