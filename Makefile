all: Client.cs Database.cs Launcher.cs CharacterLoadDialog.cs CharacterCreateDialog.cs CharacterData.cs
	dmcs -debug -r:build/Npgsql.dll,System.Drawing,System.Data,System.Windows.Forms Client.cs Database.cs Launcher.cs CharacterLoadDialog.cs CharacterCreateDialog.cs CharacterData.cs -out:build/Gurps.exe
clean:
	rm build/*
